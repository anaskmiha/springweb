# Utiliser une image de base contenant Java
FROM adoptopenjdk:11-jdk-hotspot

# Définir le répertoire de travail dans le conteneur
WORKDIR /app

# Copier le fichier JAR de l'application dans le conteneur
COPY target/Spring-0.0.1-SNAPSHOT.jar app.jar

# Exposer le port sur lequel l'application écoute
EXPOSE 8080

# Commande pour exécuter l'application lorsque le conteneur démarre
CMD ["java", "-jar", "app.jar"]
